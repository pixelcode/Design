# Codeberg Logo Kit (LEGACY VERSION)

**WARNING: DO NOT TOUCH THIS DIRECTORY!!!**
It is generated automatically by `../logo-kit-v2/copy-to-legacy-version.sh` and
exists for legacy reasons, as some websites still use the old URL schema.

Please refer to [../logo-kit-v2/README.md](../logo-kit-v2/README.md) for more
information.

