# Codeberg Logo Kit

## PLEASE DO THESE THINGS

- Use the logo to link to your Codeberg repository
- Pick and chose the best fit from the Logo-Kit, make sure to always use the white version on dark backgrounds.
- Print the logo on t-shirts and mugs because you think we are great :D


## PLEASE, DON'T DO THESE THINGS

- Don't use the special version (with fake transparency) unless you are technically forced to do so.
- Deform the logo by stretching, compressing or skewing
- Use theblue variant on dark backgrunds
- Tint the logo in a custom color
- Put the logo on a background that is noisy or has a poor contrast
- Detach and rearrange the logo symbol & logotype
- Add a second color to the logo


© COPYRIGHT

Logo material is licensed under CC0 http://creativecommons.org/publicdomain/zero/1.0/
Codeberg and the Codeberg Logo are trademarks of Codeberg e.V

## GET IN TOUCH
If you have questions about usage please just get in touch with us. Also if you miss any particular format or material to promote Codeberg just get in touch with us!
