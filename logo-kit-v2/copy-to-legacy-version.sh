#!/bin/sh
set -eu
rm -rf ../logo-kit
mkdir -p ../logo-kit

cp horizontal/svg/codeberg-logo_horizontal_blue.svg ../logo-kit/horizontal.svg
cp horizontal/png/codeberg-logo_horizontal_blue-850x250.png ../logo-kit/horizontal.png
cp horizontal/svg/codeberg-logo_horizontal_white.svg ../logo-kit/horizontal_inverted.svg
cp horizontal/png/codeberg-logo_horizontal_white-850x250.png ../logo-kit/horizontal_inverted.png

cp vertical/svg/codeberg-logo_vertical_blue.svg ../logo-kit/stacked.svg
cp vertical/png/codeberg-logo_vertical_blue-512x512.png ../logo-kit/stacked.png
cp vertical/svg/codeberg-logo_vertical_white.svg ../logo-kit/stacked_inverted.svg
cp vertical/png/codeberg-logo_vertical_white-512x512.png ../logo-kit/stacked_inverted.png

cp icon/svg/codeberg-logo_icon_blue.svg ../logo-kit/icon.svg
cp icon/png/codeberg-logo_icon_blue-64x64.png ../logo-kit/icon_32px.png
cp icon/png/codeberg-logo_icon_blue-64x64.png ../logo-kit/icon_64px.png
cp icon/svg/codeberg-logo_icon_white.svg ../logo-kit/icon_inverted.svg
cp icon/png/codeberg-logo_icon_white-64x64.png ../logo-kit/icon_inverted_32px.png
cp icon/png/codeberg-logo_icon_white-64x64.png ../logo-kit/icon_inverted_64px.png

cp favicon/codeberg-logo_favicon.svg ../logo-kit/favicon.svg
cp favicon/codeberg-logo_favicon_blue.ico ../logo-kit/favicon.ico
cp favicon/codeberg-logo_apple-touch-icon.png ../logo-kit/apple-touch-icon.png

cat <<'EOF' > ../logo-kit/_README_DO_NOT_TOUCH_THIS_DIRECTORY.md
# Codeberg Logo Kit (LEGACY VERSION)

**WARNING: DO NOT TOUCH THIS DIRECTORY!!!**
It is generated automatically by `../logo-kit-v2/copy-to-legacy-version.sh` and
exists for legacy reasons, as some websites still use the old URL schema.

Please refer to [../logo-kit-v2/README.md](../logo-kit-v2/README.md) for more
information.

EOF
