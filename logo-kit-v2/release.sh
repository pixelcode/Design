#!/bin/sh
set -eu
archive=Codeberg-Logo-Kit.zip
rm -f "$archive"
zip -r "$archive" horizontal vertical icon favicon special README.md
( cd .. && zip logo-kit-v2/"$archive" LICENSE; )
